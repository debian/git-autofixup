git-autofixup (0.004006-1) unstable; urgency=medium

  [ Jordan Torbiak ]
  * Remove unused import
  * Skip testing backslashes in filenames on cygwin
  * Skip tests when perl is for Cygwin but git is for msys
  * Fix file-in-use issue on Windows
  * Load Pod::Usage at runtime since Git for Windows doesn't include it
  * Describe Windows support better
  * Fallback to --set-upstream when --set-upstream-to isn't available
  * child_error_desc: use the given error
  * Break the circular dependency between Util and Repo
  * Print git info on test failure, for a particular test
  * Skip testing backslashes in filenames under msys
  * Check that notes are added for the latest release
  * Version 0.004002
  * Automatically update MANIFEST during release
  * Version 0.004003
  * Skip capture() tests on Windows
  * Move the special diagnostics to the correct test
  * Make Repo::set_upstream() less noisy
  * Fix upstream mismatch due to missing --fork-point mode
  * Version 0.004004
  * Fix --help
  * Add --gitopt deprecation note to the manual
  * Version 0.004005
  * xt/pod.t: only check files that have POD
  * Fix some cleanup issues in repo.pl
  * Add core modules as dependencies
  * Version 0.004006

 -- Daniel Gröber <dxld@darkboxed.org>  Mon, 06 May 2024 20:41:54 +0200

git-autofixup (0.004001-1) unstable; urgency=medium

  [ Daniel Gröber ]
  * New upstream release
  * Fix Vcs URLs

  [ Johannes Altmanninger ]
  * Handle filenames that contain spaces

  [ Johannes Altmanninger ]
  * Suppress Git warning about implicit default branch

  [ Jordan Torbiak ]
  * Have `git blame` only search back to upstream

  [ Johannes Altmanninger ]
  * Speed up creation of temporary index when autofixing staged changes

  [ Jordan Torbiak ]
  * Support quoted filenames in diff output
  * Supply initial branch name via env
  * Choose upstream revisions automatically
  * Be more precise when checking if $strict is valid
  * Improve error message for invalid number of context lines
  * Deprecate --gitopt|-g
  * Improve error handling for external/system commands

 -- Daniel Gröber <dxld@darkboxed.org>  Sat, 23 Sep 2023 23:20:39 +0200

git-autofixup (0.003001-2) unstable; urgency=medium

  * Mark as M-A:foreign

 -- Daniel Gröber <dxld@darkboxed.org>  Sat, 22 Jan 2022 00:58:40 +0100

git-autofixup (0.003001-1) unstable; urgency=medium

  * Initial Release. (Closes: #987884)

 -- Daniel Gröber <dxld@darkboxed.org>  Sat, 01 May 2021 12:58:53 +0200
